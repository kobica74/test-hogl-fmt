#include <iostream>

// https://en.cppreference.com/w/cpp/header/cinttypes
#include <cinttypes>
#include <memory>
#include <vector>
#include <memory>
#include <chrono>
#include <iomanip>
#include <vector>
#include <thread>

#include <fmt/format.h>

#include "hogl/output-textfile.hpp"
#include "hogl/format-basic.hpp"
#include "hogl/post.hpp"
#include "hogl/engine.hpp"
#include "hogl/area.hpp"
#include "hogl/output-textfile.hpp"
#include "hogl/format-basic.hpp"
#include "hogl/mask.hpp"

namespace {

    std::string generate_output_file(const std::string &base_name, const std::string &extension) {
        auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        auto date_time = std::put_time(localtime(&now), "%T"); // <-- this is what is causing this strdup to fail. why?
        std::ostringstream dt;
        dt << date_time;
        std::string fmt_dt = dt.str();
        std::replace(std::begin(fmt_dt), std::end(fmt_dt), ':', '-');
        return base_name + fmt_dt + extension;
    }

    const char* ping_section_names[] = {
            "DEBUG",
            "INFO",
            "WARN",
            "ERROR"
    };

    enum section_id {
        DEBUG,
        INFO,
        WARN,
        ERROR
    };

    class logger final {
    public:

        // hogl needs this as public
        const hogl::area *_area = nullptr;

        void init(const std::string &filename, const std::vector<std::string> &logmask) {
            std::unique_ptr<hogl::format> format = std::make_unique<hogl::format_basic>(_format_type.c_str());
            _format = std::move(format);
            auto output_bufsize = 10 * 1024 * 1024;
            std::string output_file = ::generate_output_file(filename, _extention);
            std::unique_ptr<hogl::output> output = std::make_unique<hogl::output_textfile>(output_file.c_str(), *_format, output_bufsize);
            _output = std::move(output);
            hogl::mask mask;
            auto append_mask = [&mask](const auto &s){ mask << s; };
            (void)std::for_each(std::begin(logmask), std::end(logmask), append_mask);
            hogl::activate(*_output);
            std::string ping = "PING";
            strdup(ping.c_str());
            _area = hogl::add_area(ping.c_str(), ping_section_names);
            hogl::apply_mask(mask);
        }

        void stop() {
            hogl::deactivate();
        }
    private:
        static constexpr char _extention[] = ".log";
        std::string _format_type = "timestamp|timedelta|ring|seqnum|area|section";
        std::unique_ptr<hogl::format> _format;
        std::unique_ptr<hogl::output> _output;
    };

}

int main() {
    logger log;

    std::clog << fmt::format("hello world {0}\n", "!");

    log.init("sample", {".*:.*"});

    hogl::post(log._area, DEBUG, "hello world");

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(1s);

    log.stop();

    return 0;
}